from uuid import UUID, uuid4

from fastapi import FastAPI, Request, Form
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from pydantic import BaseModel


app = FastAPI()
templates = Jinja2Templates("templates")


class Book(BaseModel):
    id: UUID
    title: str
    author: str


# Imagine that this is a database
books = {
    book.id: book for book in [
        Book(id=uuid4(), title="1984", author="George Orwell"),
        Book(id=uuid4(), title="Pippi Långstrump", author="Astrid Lindgren"),
        Book(id=uuid4(), title="The GNU Manifesto", author="Richard M. Stallman"),
        Book(id=uuid4(), title="A Short History of Nearly Everything", author="Bill Bryson"),
    ]
}


@app.get("/", response_class=HTMLResponse)
async def index(request: Request):
    return templates.TemplateResponse("index.j2.html", {"request": request, "books": books.values()})


@app.post("/submit", response_class=HTMLResponse)
async def add_book(request: Request, title: str = Form(...), author: str = Form(...)):
    book = Book(id=uuid4(), title=title, author=author)
    books[book.id] = book
    return templates.TemplateResponse("book.j2.html", {"request": request, "book": book})


@app.delete("/delete/{id}", response_class=HTMLResponse)
async def delete_book(id: UUID):
    del books[id]
    return ""
