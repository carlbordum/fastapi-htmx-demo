# fastapi-htmx-demo

This is a small demo using [htmx](https://htmx.org/) together with
[FastAPI](https://fastapi.tiangolo.com/) and
[jinja2](https://jinja.palletsprojects.com/).

You can get a lot done with very little, especially if we made a package that
makes jinja and FastAPI more ergonomic together and improves jinjas includes.

To try it out, run `poetry run uvicorn --reload app:app`
